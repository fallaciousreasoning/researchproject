import sys
import os

from PIL import Image

def convert_image(input_file, output_file):
	img = Image.open(input_file).convert('L')
	img.save(output_file)

def convert_images(input_folder, output_folder):
	files = os.listdir(input_folder)
	if not os.path.exists(output_folder):
		os.mkdir(output_folder)

	total = len(files)
	done = 0

	for file in files:
		if len(file) > 4 and file[-4:] == ".png":
			convert_image(input_folder + "\\" + file, output_folder + "\\" + file)

		done += 1
		sys.stdout.write('\rDone ' + str(done) + ' of ' + str(total) +' files (' + str(done/total*100) +'%)')
		sys.stdout.flush()

if __name__ == '__main__':
	args = sys.argv

	if len(args) < 3:
		print("An input and output folder must be provided!")
		print("Correct usage is python color2gray.py [input_folder] [output_folder]")
		sys.exit(-1)

	convert_images(args[1], args[2])