import sys
import os

from PIL import Image
import numpy as np

import _pickle as pickle
import matplotlib.pyplot as plt

def compress_images(input_dir, output_file, num=-1):
    files = [file for file in os.listdir(input_dir) if file.endswith('.png')]
    
    if num != -1:
        files = files[:min(len(files), num)]

    width, height = None, None
    data = []

    print('Loading...')

    total = len(files)
    done = 0

    for file in files:
        print(file)
        image = Image.open(input_dir + "\\" + file).convert('L')
        i = []
        image_array = []
        i.append(image_array)
        
        for y in range(image.height):
            row = []
            for x in range(image.width):
                gray = image.getpixel((x, y))
                row.append(gray / 255.0)

            image_array.append(row)

        data.append(i)        

        done += 1
        sys.stdout.write('\r' + 'Loaded ' + str(done) + ' of ' + str(total) + ' (' + str(round(done/total * 100)) +  '%)')

    print('Saving...')
    pickle.dump(np.array(data, np.float32), open(output_file, 'wb'))
    print('Done.')

def load_compressed(input_file):
    return pickle.load(open(input_file, 'rb'))

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Not enough arugments')
        print('Expected usage: compacter.py [input_dir] [output_file] :[num_to_compact]')

    print(sys.argv)
    num=-1
    if len(sys.argv) > 3:
        num = int(sys.argv[3])
        print(num)

    compress_images(sys.argv[1], sys.argv[2], num)
    data = load_compressed(sys.argv[2])
    print(data.shape)

    # compress_images('..\\data\\positives_48x48_gray', 'foo.dat')
    # data = load_compressed('foo.dat')
    # plt.imshow(data[0][0])
    # plt.show()

