import sys
import os

from PIL import Image

def resize_image(input_file, output_file, new_size):
    img = Image.open(input_file).resize((new_size, new_size))
    img.save(output_file)

def resize_images(input_folder, output_folder, new_size):
    files = os.listdir(input_folder)
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    total = len(files)
    done = 0

    for file in files:
        if len(file) > 4 and file[-4:] == ".png":
            resize_image(input_folder + "\\" + file, output_folder + "\\" + file, new_size)

        done += 1
        sys.stdout.write('\rDone ' + str(done) + ' of ' + str(total) +' files (' + str(done/total*100) +'%)')
        sys.stdout.flush()

if __name__ == '__main__':
    args = sys.argv

    if len(args) < 3:
        print("An input and output folder must be provided!")
        print("Correct usage is python resizer.py [input_folder] [output_folder] [size]")
        sys.exit(-1)

    resize_images(args[1], args[2], int(args[3]))