import sys
import random

import os
import math

from PIL import Image
import uuid

OUTPUT_SIZE = 64

def generate_window(image, output_dir, output_size):
	min_dimension = min(image.width, image.height)

	size = random.randrange(output_size, min_dimension)
	x = random.randrange(0, image.width - size + 1)
	y = random.randrange(0, image.height - size + 1)

	filename = output_dir + "\\" + str(uuid.uuid4()) + '.png'
	image.crop((x, y, x + size, y + size)).resize((output_size, output_size), Image.ANTIALIAS).save(filename)

def generate_windows(input_dir, output_dir, generate_per_image, output_size=OUTPUT_SIZE):
	if not os.path.exists(output_dir):
		os.mkdir(output_dir)

	files = [file for file in os.listdir(input_dir) if file[-4:] == '.png']
	done = 0

	total = generate_per_image * len(files)

	for file in files:
		image = Image.open(input_dir + '\\' + file)

		for i in range(generate_per_image):
			generate_window(image, output_dir, output_size)
			done += 1
			sys.stdout.write('\rGenerating... (' + str(round((done / total) * 100)) + '% complete)')

if __name__ == '__main__':
	if len(sys.argv) < 4:
		print('Missing arguments!')
		print('Correct usage is: negative_extractor.py [input_dir] [output_dir] [generate_per_image]')

	generate_windows(sys.argv[1], sys.argv[2], int(sys.argv[3]))



