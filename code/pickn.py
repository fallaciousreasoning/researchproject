import sys

import os

import random
from shutil import copyfile


def pick(input_folder, output_folder, n):

    files = os.listdir(input_folder)

    selected = []

    while len(selected) < n:
        i = random.randint(0, len(files))
        selected.append(files.pop(i))

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for file in selected:
        copyfile(input_folder + '\\' + file, output_folder + '\\' + file)

if __name__ == '__main__':
    input_folder = sys.argv[1]
    output_folder = sys.argv[2]

    n = 10000
    if len(sys.argv) > 3:
        n = int(sys.argv[3])

    pick(input_folder, output_folder, n)

    