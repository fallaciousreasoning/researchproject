from detector import get_detector
from Box import Box

import os

MIN_BUILDING_AREA = 576

def evaluate(tiles_folder, results_folder, ground_truths=None, max_count=None):
    detector = get_detector()
    detector.output_folder = results_folder

    images = os.listdir(tiles_folder)

    if max_count is None: max_count = len(images)

    tp = 0
    fp = 0

    total_buildings = 0
    detected_buildings = 0

    for i in range(max_count):
        image = images[i]
        name = filename_minus_extension(image)

        # If we've been given some ground truths but none for this image, don't evaluate it
        if ground_truths is not None and name not in ground_truths: continue

        results = detector.detect(tiles_folder + '\\' + image)

        truths = ground_truths[name] if ground_truths is not None else []
        truths = [truth for truth in truths if truth.width * truth.height > MIN_BUILDING_AREA]
        # for truth in truths:
        #     print(truth.width * truth.height)

        detected = set()

        for x, y, width, height in results:
            box = Box(x, y, width, height)

            is_truth = False
            for truth in truths:
                if truth.intersects(box):
                    is_truth = True

                    detected.update({(int(truth.x), int(truth.y), int(truth.width), int(truth.height))})
                    break

            if is_truth:
                tp += 1
            else:
                fp += 1

        total_buildings += len(truths)
        detected_buildings += len(detected)

        with open('results.txt', 'w') as f:
            f.write('tp: ' + str(tp) + '\n')
            f.write('fp: ' + str(fp) + '\n')

            f.write('total_buildings: ' + str(total_buildings) + '\n')
            f.write('detected_buildings: ' + str(detected_buildings) + '\n')

        print('Done ' + str(i) + ' of ' + str(len(images)))


def load_ground_truths(path):
    result = {}

    truths_files = os.listdir(path)

    for file in truths_files:
        name = filename_minus_extension(file)

        result[name] = []

        with open(path + '\\' + file) as f:
            lines = f.readlines()
            for line in lines:
                x, y, width, height = [float(x) for x in line.split(' ')]
                result[name].append(Box(x, y, width, height))

    return result


def filename_minus_extension(file_path):
    basename = os.path.basename(file_path)
    minus_extension = os.path.splitext(basename)[0]

    return minus_extension


if __name__ == '__main__':
    test_input = 'C:\\Users\\myadmin.CS13013JL\\joh19\\Tiles'
    results_folder = 'C:\\Users\\myadmin.CS13013JL\\joh19\\Results'
    truths_folder = 'C:\\Users\\myadmin.CS13013JL\\joh19\\BuildingBoxes'

    load_truths = True
    max_count = 200

    evaluate(test_input, results_folder, None if not load_truths else load_ground_truths(truths_folder), max_count=max_count)
