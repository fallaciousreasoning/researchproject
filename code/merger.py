import sys

import numpy as np

import _pickle as pickle
import compacter

def merge(files, output_file):
    result = []

    for file in files:
        data = compacter.load_compressed(file)
        for sample in data:
            result.append(sample)

    result = np.array(result, dtype=np.float32)
    print(result.shape)

    print('Saving...')
    pickle.dump(result, open(output_file, 'wb'))
    print('Done.')

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Not enough arguments. Shoudl be [input_files....] [output file]')
        sys.exit(-1)

    files = sys.argv[1:-1]
    output = sys.argv[-1]
    print(files)
    merge(files, output)