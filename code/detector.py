import sys
import lasagne

import numpy as np

from PIL import Image, ImageDraw

from sliding_window import SlidingWindow
from building_trainer import build_cnn
from building_mlp import build_mlp

import theano
import theano.tensor as T

import os
import ntpath

class Detector:
    def __init__(self, net, output_folder='C:\\Users\\myadmin.CS13013JL\\joh19\\Results'):
        self.net = net
        self.output_folder = output_folder

        if not os.path.exists(output_folder): os.makedirs(output_folder)

    def detect(self, image_path):
        name = ntpath.basename(image_path)[:-4]

        col_image = Image.open(image_path)
        image = col_image.convert('L')
        slider = SlidingWindow(image)

        results = []

        input_var = T.tensor4('inputs')
        pred = lasagne.layers.get_output(self.net, input_var, deterministic=True)
        get_pred = theano.function([input_var], pred)
        count = 0
        for rectangle, window in slider:
            input = []
            image_array = []
            input.append(image_array)

            for y in range(window.height):
                row = []
                for x in range(window.width):
                    gray = window.getpixel((x, y))
                    row.append(gray / 255.0)

                image_array.append(row)

            input = np.array([input])
            result = get_pred(input)

            if result[0][0] < result[0][1]:
                print("Found Building?", rectangle)
                results.append(rectangle)
                count += 1
                #window.save(self.output_folder + "\\" + str(rectangle) + '.png')

        draw = ImageDraw.Draw(col_image)
        for x, y, width, height in results:
            draw.rectangle([x, y, x + width, y + height], outline='red')

        col_image.save(self.output_folder + '\\' + name + '_results.png')

        with open(self.output_folder + '\\' + name + '.txt', 'w') as f:
            for x, y, width, height in results:
                f.write(str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + '\n')

        print('Found', count, "buildings")
        return results

def get_detector(mode='cnn'):
    net = build_cnn() if mode == 'cnn' else build_mlp()

    with np.load(mode + '_model.npz') as f:
        param_values = [f['arr_%d' % i] for i in range(len(f.files))]

    lasagne.layers.set_all_param_values(net, param_values)

    detector = Detector(net)
    return detector

if __name__ == '__main__':
    # 0a33533a-bae0-4b01-aab0-1fa7ae316dd7
    print('\n\n')
    if len(sys.argv) < 2:
        print("Not enough arguments!")
        sys.exit(-1)

    detector = get_detector()
    detector.detect(sys.argv[1])
