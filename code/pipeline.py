import color2gray
import compacter
import joiner
import negative_extractor
import resizer

RAW_NEGATIVES_DIR = '..\\data\\negatives'
RAW_POSITIVES_DIR = '..\\data\\positives_64x64'

EXTRACTED_SUFFIX = '_extracted'
GRAY_SUFFIX = '_gray'

SIZE = 48

NEGATIVES_PER_RAW = 10

NEGATIVES_DATA = 'negatives.data'
POSITIVES_DATA = 'positives.data'

SAMPLES_DATA = 'samples.data'

def pipe_extracted(input_dir, data_file):
    gray_dir = input_dir + GRAY_SUFFIX

    print("Sapping Color....")
    color2gray.convert_images(input_dir, gray_dir)
    print("Sapped!")

    print("Resizing.....")
    resizer.resize_images(gray_dir, gray_dir, SIZE)
    print("Resized!")

    print("Compacting....")
    compacter.compress_images(gray_dir, data_file)
    print("Compacted!")

def run():
    print("Running Positive Pipe...")
    pipe_extracted(RAW_POSITIVES_DIR, POSITIVES_DATA)
    print("Done!")

    print("Running Negative Pipe...")

    print("Extracting Negatives...")
    extracted_negatives = RAW_NEGATIVES_DIR + EXTRACTED_SUFFIX
    negative_extractor.generate_windows(RAW_NEGATIVES_DIR, extracted_negatives, NEGATIVES_PER_RAW)
    print("Extracted!")

    pipe_extracted(extracted_negatives, NEGATIVES_DATA)
    print("Done!")

    print("Joining...")

    print("loading data...")
    data = joiner.load(POSITIVES_DATA, NEGATIVES_DATA)
    print("loaded!")

    print("saving...")
    joiner.save_data(data, SAMPLES_DATA)
    print("Saved!")

    print("Joined!")
    print("Done!")

if __name__ == '__main__':
    run()