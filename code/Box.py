class Box:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def mn(self, newValue=None):
        if newValue is None:
            return self.x, self.y

        self.x = newValue[0]
        self.y = newValue[1]

    def mx(self, newValue=None):
        if newValue is None:
            return self.x + self.width, self.y + self.height

        self.width = newValue[0] - self.x
        self.height = newValue[1] - self.y

    def centre(self):
        mn = self.min()
        mx = self.max()

        return (mn[0] + mx[0]) * 0.5, (mx[1] + mn[1]) * 0.5

    def intersects(self, other):
        return not (self.x > other.mx()[0] or self.y > other.mx()[1] or self.mx()[0] < other.x or self.mx()[1] < other.y)

    def overlap(self, other):
        x_intersection = min(self.mx()[0], other.mx()[0]) - max(self.x, other.x)
        y_intersection = min(self.mx[1], other.mx[1]) - max(self.y, other.y);

        if x_intersection < 0 or y_intersection < 0:
            return 0

        return x_intersection * y_intersection;
