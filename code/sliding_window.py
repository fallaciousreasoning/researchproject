from PIL import Image

class SlidingWindow:
    def __init__(self, image, window_width=48, window_height=48, scale_step=0.8, x_step=26, y_step=26):
        self.image = image
        self.window_width = window_width
        self.window_height = window_height
        self.scale_step = scale_step
        self.x_step = int(window_width/2) if x_step is None else x_step
        self.y_step = int(window_height/2) if y_step is None else y_step

    def __iter__(self):
        scale = 1

        max_window_dimension = max(self.window_width, self.window_width)

        while True:
            image = self.image.resize((int(self.image.width*scale), int(self.image.height*scale)), Image.ANTIALIAS)
            if image.width < self.window_width or image.height < self.window_height: break

            for x in range(0, image.width - self.x_step, self.x_step):
                for y in range(0, image.height - self.y_step, self.y_step):
                    if (x + self.window_width)/scale > self.image.width or (y + self.window_height)/scale > self.image.height: continue

                    rectangle = (x, y, x+self.window_width, y+self.window_height)
                    scaled_rectangle = (int(x / scale), int(y / scale), int(self.window_width / scale), int(self.window_height / scale))

                    yield scaled_rectangle, self.image.crop(rectangle)

            scale *= self.scale_step

if __name__ == '__main__':
    image = Image.open('test.png')
    slider = SlidingWindow(image)

    count = 0
    for rectangle, window in slider:
        print(rectangle)
        count += 1

    print(count)
