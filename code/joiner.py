import sys

import compacter
import numpy as np

import _pickle as pickle
import sPickle

import os
import psutil

def load(positive_file, negative_file):
    """Loads the data from the positive and negative file and puts it into the format we want"""

    #load the positie and negative file
    positives = compacter.load_compressed(positive_file)
    negatives = compacter.load_compressed(negative_file)

    #convert them to numpy arrays
    positives = np.array(positives, np.float32)
    negatives = np.array(negatives, np.float32)

    #generate the labels for the positive and negative files
    positive_labels = np.ones(len(positives), np.uint8)
    negative_labels = np.zeros(len(negatives), np.uint8)

    #join the samples and labels for positives and negatives
    samples = np.concatenate((positives, negatives), axis=0)
    labels = np.concatenate((positive_labels, negative_labels), axis=0)

    #shuffle the arrays, so we don't have all the positives and then all the negatives
    shuffle_in_unison(samples, labels)

    return samples, labels

def shuffle_in_unison(a, b):
    """Shuffles two related arrays so that elements continue to correspond"""
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)

def save_data(data, output_file):
    """Dumps the data using pickle"""

    pickle.dump(data, open(output_file, 'wb'))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('Not enough args')
        print('Correct usage is %s [positive_file] [negative_file] [output_file]' % sys.argv[0])
        sys.exit(-1)

    try:
        data = load(sys.argv[1], sys.argv[2])
        save_data(data, sys.argv[3])
    except:
        print("Failed")

    max_mem = psutil.Process(os.getpid()).memory_info_ex().peak_wset
    print("Max Memory:", str(max_mem/1000000) + "mb")
