from PIL import Image
import _pickle as pickle

import os

import numpy as np

if not os.path.exists('recovered'):
    os.mkdir('recovered')

samples = pickle.load(open('samples.data', 'rb'))

i = -10
for sample in samples[0][i:]:
    print(str(i) + ': ' + ('building' if samples[1][i] == 1 else 'negative'))
    data = sample[0]

    data = np.multiply(data, 255)
    data = np.reshape(data, (48*48))

    image = Image.new('L', (48,48))
    image.putdata(data)

    image.save('recovered\\' + str(i) + '.png')
    i+=1



