﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;

namespace NumberRecognizer
{
    public class Image
    {
        public int Width => Pixels.GetLength(0);
        public int Height => Pixels.GetLength(1);

        public double[,] Pixels { get; set; }

        public double[] RawPixels
        {
            get
            {
                var array = new double[Width*Height];
                for (var x = 0; x < Width; ++x)
                    for (var y = 0; y < Height; ++y)
                        array[x + y*Width] = Pixels[x, y];
                return array;
            }
        }
    }
}
