﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Neuro;
using Accord.Neuro.Learning;
using Accord.Neuro.Networks;
using AForge.Neuro.Learning;

namespace NumberRecognizer
{
    class Program
    {
        static void Main(string[] args)
        {
            var trainingLabels = LabelParser.ParseLabels("data\\train_labels.idx1-ubyte");
            var trainingImages = ImageParser.ParseImages("data\\train_images.idx3-ubyte");

            var testLabels = LabelParser.ParseLabels("data\\test_labels.idx1-ubyte");
            var testImages = ImageParser.ParseImages("data\\test_images.idx3-ubyte");

            double[][] inputs;
            double[][] outputs;
            double[][] testInputs;
            double[][] testOutputs;

            inputs = (from image in trainingImages select image.RawPixels).ToArray();
            outputs = (from label in trainingLabels select SeperateExpectedOutput(label)).ToArray();

            testInputs = (from image in testImages select image.RawPixels).ToArray();
            testOutputs = (from label in testLabels select SeperateExpectedOutput(label)).ToArray();

            var network = new DeepBeliefNetwork(inputs.First().Length, 10, 10, 10);
            new GaussianWeights(network, 0.1).Randomize();
            network.UpdateVisibleWeights();

            // Supervised learning on entire network, to provide output classification.
            var teacher2 = new BackPropagationLearning(network)
            {
                LearningRate = 0.1,
                Momentum = 0.5
            };

            // Run supervised learning.
            for (int i = 0; i < 30; i++)
            {
                double error = teacher2.RunEpoch(inputs, outputs)/inputs.Length;
                if (i%10 == 0)
                {
                    Console.WriteLine(i + ", Error = " + error);
                }
            }


            // Test the resulting accuracy.
            int correct = 0;
            for (int i = 0; i < inputs.Length; i++)
            {
                double[] outputValues = network.Compute(inputs[i]);
                //Check if we're close enough
                if (Matches(outputValues, outputs[i]))
                {
                    correct++;
                }
            }

            using (var stream = File.Create("buildingdetector.nn"))
            {
                network.Save(stream);
            }

            Console.WriteLine("Correct " + correct + "/" + inputs.Length + ", " +
                              Math.Round(((double)correct / (double)inputs.Length * 100), 2) + "%");

            Console.WriteLine("Done. Press any key to exit");
            Console.ReadKey();
        }

        private static bool Matches(double[] output, double[] expected)
        {
            var maxIndex = 0;
            for (int i = 0; i < output.Length; i++)
            {
                if (output[i] >= output[maxIndex]) maxIndex = i;
            }

            return (int) expected[maxIndex] == 0;
        }

        private static double[] SeperateExpectedOutput(double expected)
        {
            int expectedInt = (int) expected;

            var result = new double[10];
            for (var i = 0; i < result.Length; ++i)
            {
                if (i == expectedInt) result[i]++;
                else result[i]--;
            }

            return result;
        }
    }
}
