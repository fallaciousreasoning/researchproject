﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberRecognizer
{
    public class ImageParser
    {
        public static List<Image> ParseImages(string file)
        {
            var images = new List<Image>();

            using (var reader = new BinaryReader(File.OpenRead(file)))
            {
                var magicNumber = reader.ReadBigEndianInt32();
                var numImages = reader.ReadBigEndianInt32();

                var width = reader.ReadBigEndianInt32();
                var height = reader.ReadBigEndianInt32();

                for (var i = 0; i < numImages; ++i)
                {
                    var image = new Image();
                    var pixels = new double[width, height];

                    for (var y = 0; y < height; ++y)
                        for (var x = 0; x < width; ++x)
                        {
                            var pixel = reader.ReadByte();
                            pixels[x, y] = pixel;
                        }

                    image.Pixels = pixels;
                    images.Add(image);
                }
            }

            return images;
        }
    }
}
