﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberRecognizer
{
    public static class LabelParser
    {
        public static double[] ParseLabels(string path)
        {
            double[] labels;
            using (var reader = new BinaryReader(File.OpenRead(path)))
            {
                var magicNumber = reader.ReadBigEndianInt32();
                var items = reader.ReadBigEndianInt32();

                labels = new double[items];

                for (var i = 0; i < items; ++i)
                    labels[i] = reader.ReadByte();
            }

            return labels;
        }

        public static int ReadBigEndianInt32(this BinaryReader reader)
        {
            var bytes = reader.ReadBytes(4);
            Array.Reverse(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }
    }
}
